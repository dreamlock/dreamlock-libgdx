package gr.dreamlock.dreamlock.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import gr.dreamlock.dreamlock.models.Inventory;

public class InventoryRenderer implements IRenderer{
    private static final float CAMERA_WIDTH = 1280;
    private static final float CAMERA_HEIGHT = 720;

    private Inventory inventory;
    private OrthographicCamera camera;

    private Texture slotTexture;
    private Texture inventoryTexture;

    // test
    private Texture key;

    private SpriteBatch spriteBatch;

    public InventoryRenderer(Inventory inventory) {
        this.inventory = inventory;
        this.camera = new OrthographicCamera(CAMERA_WIDTH, CAMERA_HEIGHT);
        this.camera.position.set(CAMERA_WIDTH / 2f, CAMERA_HEIGHT / 2f, 0);
        this.camera.update();
        spriteBatch = new SpriteBatch();
        loadTextures();
    }

    // TODO
    private void loadTextures() {
        slotTexture = new Texture(Gdx.files.internal("slot.png"));
        inventoryTexture = new Texture(Gdx.files.internal("inventory.jpg"));

        key = new Texture(Gdx.files.internal("key.png"));
    }

    @Override
    public void render() {
        spriteBatch.begin();
        drawInventory();
        drawSlots();
        spriteBatch.end();
    }

    private void drawInventory() {
        spriteBatch.draw(inventoryTexture,0,0);
    }

    private void drawSlots() {
        for (int i = 0 ; i < Inventory.INVENTORY_ROWS; i++){
            for (int j = 0 ; j < Inventory.INVENTORY_COLUMNS; j++){
                spriteBatch.draw(slotTexture, inventory.getSlots()[i][j].getPosition().x, 720 - inventory.getSlots()[i][j].getPosition().y);
                if (!inventory.getSlots()[i][j].isEmpty()) {
                    spriteBatch.draw(key, inventory.getSlots()[i][j].getPosition().x + 10, 720 - inventory.getSlots()[i][j].getPosition().y + 10);
                }
            }
        }
    }
}
