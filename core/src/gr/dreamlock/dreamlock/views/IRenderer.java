package gr.dreamlock.dreamlock.views;

public interface IRenderer {
    void render();
}
