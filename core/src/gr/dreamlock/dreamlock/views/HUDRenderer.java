package gr.dreamlock.dreamlock.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import gr.dreamlock.dreamlock.models.HUD;

/**
 * Created by Veruz on 12/3/2015.
 */
public class HUDRenderer {
    private static final float CAMERA_WIDTH = 1280;
    private static final float CAMERA_HEIGHT = 720;

    private HUD hud;
    private OrthographicCamera camera;

    private Texture buttonTexture;
    private Texture backgroundTexture;

    private SpriteBatch spriteBatch;

    public HUDRenderer(HUD hud){
        this.hud = hud;
        this.camera = new OrthographicCamera(CAMERA_WIDTH, CAMERA_HEIGHT);
        this.camera.position.set(CAMERA_WIDTH / 2f, CAMERA_HEIGHT / 2f, 0);
        this.camera.update();
        spriteBatch = new SpriteBatch();
        loadTextures();
    }

    private void loadTextures(){
        buttonTexture = new Texture(Gdx.files.internal("Inventory_Bag.png"));
        backgroundTexture = new Texture(Gdx.files.internal("transparent_panel.png"));
    }

    public void render(){
        spriteBatch.begin();
        drawHUD();
        spriteBatch.end();
    }

    private void drawHUD(){
        spriteBatch.draw(buttonTexture,hud.getButtonInventory().getX(),hud.getButtonInventory().getY());
    }
}
