package gr.dreamlock.dreamlock.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import gr.dreamlock.dreamlock.models.Key;
import gr.dreamlock.dreamlock.models.levels.FirstLevel;

public class WorldRenderer implements IRenderer{
    private static final float CAMERA_WIDTH = 1280;
    private static final float CAMERA_HEIGHT = 720;

    private FirstLevel level;
    private OrthographicCamera camera;

    private Texture keyTexture;
    private Texture backgroundTexture;

    private SpriteBatch spriteBatch;
    private int width;
    private int height;
    private float ppuX;	// pixels per unit on the X axis
    private float ppuY;	// pixels per unit on the Y axis

    public void setSize (int w, int h) {
        this.width = w;
        this.height = h;
        ppuX = (float) width / CAMERA_WIDTH;
        ppuY = (float) height / CAMERA_HEIGHT;
    }

    public WorldRenderer(FirstLevel level) {
        this.level = level;
        this.camera = new OrthographicCamera(CAMERA_WIDTH, CAMERA_HEIGHT);
        this.camera.position.set(CAMERA_WIDTH / 2f, CAMERA_HEIGHT / 2f, 0);
        this.camera.update();
        spriteBatch = new SpriteBatch();
        loadTextures();
    }

    //TODO: atlas loader :)
    private void loadTextures() {
        keyTexture = new Texture(Gdx.files.internal("key.png"));
        backgroundTexture = new Texture(Gdx.files.internal("room1.jpg"));
    }

    @Override
    public void render() {
        spriteBatch.begin();
        drawObjects();
        spriteBatch.end();
    }

    private void drawObjects() {
        spriteBatch.draw(backgroundTexture,0,0);
        for (Key key : level.getLevelObjects()) {
            spriteBatch.draw(keyTexture, key.getPosition().x, key.getPosition().y);
        }
    }
}
