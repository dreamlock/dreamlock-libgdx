package gr.dreamlock.dreamlock.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;
import gr.dreamlock.dreamlock.controllers.HUDController;
import gr.dreamlock.dreamlock.controllers.InventoryController;
import gr.dreamlock.dreamlock.controllers.WorldController;
import gr.dreamlock.dreamlock.helpers.InputHandler;
import gr.dreamlock.dreamlock.models.HUD;
import gr.dreamlock.dreamlock.models.IGameObject;
import gr.dreamlock.dreamlock.models.Inventory;
import gr.dreamlock.dreamlock.models.Key;
import gr.dreamlock.dreamlock.models.levels.FirstLevel;
import gr.dreamlock.dreamlock.views.HUDRenderer;
import gr.dreamlock.dreamlock.views.InventoryRenderer;
import gr.dreamlock.dreamlock.views.WorldRenderer;

public class GameScreen implements Screen {
    private FirstLevel level;
    private WorldRenderer worldRenderer;
    private WorldController worldController;
    private InventoryRenderer inventoryRenderer;
    private InventoryController inventoryController;
    private Inventory inventory;
    private HUDRenderer hudRenderer;
    private HUDController hudController;
    private HUD hud;

    private int width, height;

    @Override
    public void show() {
        // world construction
        level = new FirstLevel();
        worldRenderer = new WorldRenderer(level);
        worldController = new WorldController(level);
        // inventory construction
        inventory = new Inventory();
        inventoryRenderer = new InventoryRenderer(inventory);
        inventoryController = new InventoryController(inventory);
        hud = new HUD();
        hudRenderer = new HUDRenderer(hud);
        hudController = new HUDController(hud);
//        IGameObject gameObject = new Key(new Vector2(0,0));
//        IGameObject gameObject2 = new Key(new Vector2(0,0));
//        inventoryController.addItem(gameObject);
//        inventoryController.addItem(gameObject2);
//        inventoryController.removeItem(gameObject);

        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(new InputHandler(inventoryController));
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

//        inventoryController.update(delta);
//        inventoryRenderer.render();

        worldController.update(delta);
        worldRenderer.render();
        hudController.update(delta);
        hudRenderer.render();
    }

    @Override
    public void resize(int width, int height) {
        worldRenderer.setSize(width, height);
        this.width = width;
        this.height = height;
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        Gdx.input.setInputProcessor(null);
    }
}
