package gr.dreamlock.dreamlock.controllers;

import gr.dreamlock.dreamlock.models.IGameObject;
import gr.dreamlock.dreamlock.models.Inventory;
import gr.dreamlock.dreamlock.models.Slot;

public class InventoryController implements IController{
    private Inventory inventory;
    private Slot[][] slots;

    public InventoryController(Inventory inventory) {
        this.inventory = inventory;
        slots = inventory.getSlots();
    }

    public void update(float delta){
        processInput();
    }

    private void processInput() {

    }

    @Override
    public float touched(int screenX, int screenY) {
        return 0;
    }

    public void addItem(IGameObject gameObject) {
        int i = 0;
        int j = 0;
        Boolean flag = false ;
        do {
            while (j < Inventory.INVENTORY_COLUMNS && !flag) {
                if (slots[i][j].isEmpty()) {
                    slots[i][j].setGameObject(gameObject);
                    slots[i][j].setEmpty(false);
                    flag = true;
                }
                j++;
            }
            i++;
        } while(i < Inventory.INVENTORY_ROWS && !flag);
    }
    
    public void removeItem(IGameObject gameObject) {
        for (int i = 0 ; i < Inventory.INVENTORY_ROWS ; i++) {
            for (int j = 0 ; j < Inventory.INVENTORY_COLUMNS ; j++) {
                if (!(slots[i][j].isEmpty())) {
                    if (slots[i][j].getGameObject().equals(gameObject)) {
                        slots[i][j].setEmpty(true);
                    }
                }
            }
        }
    }
}
