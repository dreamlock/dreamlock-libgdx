package gr.dreamlock.dreamlock.controllers;

import gr.dreamlock.dreamlock.models.HUD;

public class HUDController implements IController{
    HUD hud;

    public HUDController(HUD hud) {
        this.hud=hud;
    }
    public void update(float delta){
        processInput();
    }

    private void processInput() {

    }
    @Override
    public float touched(int screenX, int screenY) {
        return 0;
    }
}
