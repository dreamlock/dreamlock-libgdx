package gr.dreamlock.dreamlock.controllers;

public interface IController {
    float touched(int screenX, int screenY);
}
