package gr.dreamlock.dreamlock.controllers;

import gr.dreamlock.dreamlock.models.levels.FirstLevel;

public class WorldController implements IController{
    private static final String TAG = WorldController.class.getName();

    public FirstLevel level;

    public WorldController(FirstLevel level) {
        this.level = level;
//        this.bob = world.getBob();
    }

    private void initLevel() {
        level = new FirstLevel();
    }

    public void update(float deltaTime) {
    }

    @Override
    public float touched(int screenX, int screenY) {
        return 0;
    }
}
