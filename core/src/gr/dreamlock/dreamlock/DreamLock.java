package gr.dreamlock.dreamlock;

import com.badlogic.gdx.Game;
import gr.dreamlock.dreamlock.screens.GameScreen;

public class DreamLock extends Game {
    @Override
	public void create () {
        setScreen(new GameScreen());
    }
}