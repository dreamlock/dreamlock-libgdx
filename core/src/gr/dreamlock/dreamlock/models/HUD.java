package gr.dreamlock.dreamlock.models;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

public class HUD extends GameObject{
    public static final int WIDTH = 640;
    public static final int HEIGHT = 128;
    private Button buttonInventory;
    private Button buttonMenu;

    public HUD ()
    {
        init();
    }
    void init()
    {
        buttonInventory = new Button();
        buttonMenu = new Button();

        buttonInventory.setSize(64,64);
        buttonMenu.setSize(64,64);
        buttonInventory.setPosition(WIDTH/2-buttonInventory.getWidth(),HEIGHT/2);
        buttonMenu.setPosition(WIDTH/2+buttonMenu.getWidth(),HEIGHT/2);

        this.position=new Vector2(1280/2,0);
    }

    public Button getButtonInventory() {
        return buttonInventory;
    }

    public Button getButtonMenu() {
        return buttonMenu;
    }
}
