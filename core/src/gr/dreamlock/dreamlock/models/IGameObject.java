package gr.dreamlock.dreamlock.models;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tommy on 12/3/2015.
 */
public interface IGameObject{
    public String getName();
    public void setName(String name);
    public String getDescription();
    public void setDescription(String description);
    public Vector2 getPosition();
    public void setPosition(Vector2 position);
}
