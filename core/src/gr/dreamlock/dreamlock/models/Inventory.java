package gr.dreamlock.dreamlock.models;


import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Inventory extends GameObject {
    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;
    public static final float VERTICAL_MARGIN = 30;
    public static final float HORIZONTAL_MARGIN = 46;
    public static final int INVENTORY_ROWS = 3;
    public static final int INVENTORY_COLUMNS = 5;

    private Slot[][] slots;
    private Rectangle bounds = new Rectangle();

    public Inventory() {
        init();
    }

    private void init() {
        this.position = new Vector2(0,0);
        this.bounds.width = WIDTH;
        this.bounds.height = HEIGHT;
        slots = new Slot[3][5];

        float x = 0;
        float y = 0;
        for (int i = 0; i < INVENTORY_ROWS; i++){
            y += VERTICAL_MARGIN;
            y += Slot.SIZE;
            for (int j = 0; j < INVENTORY_COLUMNS; j++ ){
                x += HORIZONTAL_MARGIN;
                slots[i][j] = new Slot(new Vector2(x,y));
                x += Slot.SIZE;
            }
            x = 0;
        }
    }

    public Slot[][] getSlots() {
        return slots;
    }

    public void setSlots(Slot[][] slots) {
        this.slots = slots;
    }

    public Rectangle getBounds() {
        return bounds;
    }

}
