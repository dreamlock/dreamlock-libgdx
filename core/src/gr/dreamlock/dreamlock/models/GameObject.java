package gr.dreamlock.dreamlock.models;

import com.badlogic.gdx.math.Vector2;

public abstract class GameObject implements IGameObject{
    public static final String TAG = Key.class.getName();

    String name;
    String description;
    Vector2 position;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }
}
