package gr.dreamlock.dreamlock.models;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Slot extends GameObject {
    public static final int SIZE = 200;

    private Boolean empty;
    private IGameObject gameObject;

    private Rectangle bounds = new Rectangle();

    public Slot(Vector2 position) {
        this.position = position;
        this.bounds.width = SIZE;
        this.bounds.height = SIZE;
        this.empty = true;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public Boolean isEmpty() {
        return empty;
    }

    public void setEmpty(Boolean empty) {
        this.empty = empty;
    }

    public IGameObject getGameObject() {
        return gameObject;
    }

    public void setGameObject(IGameObject gameObject) {
        this.gameObject = gameObject;
    }
}
