package gr.dreamlock.dreamlock.models.levels;

import com.badlogic.gdx.math.Vector2;
import gr.dreamlock.dreamlock.models.Key;

import java.util.ArrayList;
import java.util.List;

public class FirstLevel {
    private List<Key> objects;

    public FirstLevel() {
        createFirstLevel();
    }

    private void createFirstLevel() {
        objects = new ArrayList<Key>();
        objects.add(new Key(new Vector2(0,0)));
        objects.add(new Key(new Vector2(10,10)));
    }

    public List<Key> getLevelObjects() {
        return objects;
    }
}
