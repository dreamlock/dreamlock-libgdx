package gr.dreamlock.dreamlock.models.levels;

import gr.dreamlock.dreamlock.models.GameObject;

import java.util.List;

public interface ILevel {
    List<GameObject> getLevelObjects();
}
