package gr.dreamlock.dreamlock.models;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Key extends GameObject {
    public static final String TAG = Key.class.getName();
    public static final int WIDTH = 64;
    public static final int HEIGHT = 64;

    private Rectangle bounds = new Rectangle();

    public Key(Vector2 pos) {
        this.position = pos;
        this.bounds.width = WIDTH;
        this.bounds.height = HEIGHT;
    }

    public Rectangle getBounds() {
        return bounds;
    }
}
