package gr.dreamlock.dreamlock.helpers;

import com.badlogic.gdx.InputAdapter;
import gr.dreamlock.dreamlock.controllers.IController;

public class InputHandler extends InputAdapter{
    private IController controller;

    public InputHandler(IController controller) {
        this.controller = controller;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        controller.touched(screenX, screenY);
        System.out.println("OUCHHH-> "+screenX+":"+screenY);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        controller.touched(screenX, screenY);
        System.out.println("...it went off!!");
        return true;
    }
}
